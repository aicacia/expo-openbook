import { default as React } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { HomeScreen } from "./HomeScreen";

export type ParamList = {
  Home: undefined;
};

export const Stack = createStackNavigator<ParamList>();

export function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          cardStyle: {
            width: "100%",
            height: "100%",
            overflow: "hidden",
          },
        }}
      >
        <Stack.Screen name="Home" component={HomeScreen} options={{}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
