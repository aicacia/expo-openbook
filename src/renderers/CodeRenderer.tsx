import { default as React } from "react";
import { Text } from "react-native-paper";
import { Code } from "@aicacia/openbook";

export interface ICodeRendererProps {
  code: Code;
}

export function CodeRenderer(props: ICodeRendererProps) {
  return <Text>{props.code.getValue()}</Text>;
}
