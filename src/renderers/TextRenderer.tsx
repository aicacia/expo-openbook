import { default as React } from "react";
import { Text } from "react-native-paper";
import { Text as OpenBookText } from "@aicacia/openbook";

export interface ITextRendererProps {
  text: OpenBookText;
}

export function TextRenderer(props: ITextRendererProps) {
  return <Text>{props.text.getValue()}</Text>;
}
