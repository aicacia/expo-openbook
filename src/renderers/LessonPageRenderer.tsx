import { default as React } from "react";
import { StyleSheet } from "react-native";
import { Title, Divider } from "react-native-paper";
import { Lesson } from "@aicacia/openbook";
import { View } from "react-native";
import { RenderableRenderer } from "./RenderableRenderer";

export interface ILessonPageRendererProps {
  lesson: Lesson;
}

const styles = StyleSheet.create({
  children: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
});

export function LessonPageRenderer(props: ILessonPageRendererProps) {
  return (
    <View>
      <Title>
        <RenderableRenderer node={props.lesson.getTitle()} />
      </Title>
      <Divider />
      <View style={styles.children}>
        {props.lesson.getChildren().map((child, index) => (
          <RenderableRenderer key={index} node={child} />
        ))}
      </View>
    </View>
  );
}
