import { default as React } from "react";
import { Text } from "react-native-paper";
import { Node, Reference, Text as OpenBookText } from "@aicacia/openbook";

export interface IRenderableRendererProps {
  node: Node;
}

export function RenderableRenderer(props: IRenderableRendererProps) {
  if (props.node instanceof OpenBookText) {
    return <TextRenderer text={props.node} />;
  } else if (props.node instanceof Reference) {
    return <ReferenceRenderer reference={props.node} />;
  } else {
    return <Text>No Renderer for ${props.node} yet!</Text>;
  }
}

import { TextRenderer } from "./TextRenderer";
import { ReferenceRenderer } from "./ReferenceRenderer";
