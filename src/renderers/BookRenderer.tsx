import { default as React, useState, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { List, Surface } from "react-native-paper";
import { Book, Lesson, Example, Definition } from "@aicacia/openbook";
import { none, Option, some } from "@aicacia/core";
import { RenderableRenderer } from "./RenderableRenderer";
import { LessonPageRenderer } from "./LessonPageRenderer";
import { BookPageRenderer } from "./BookPageRenderer";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    height: "100%",
  },
  sidebar: {
    flexGrow: 1,
    minWidth: 320,
    flex: 1,
    overflow: "scroll",
  },
  main: {
    flexBasis: 999,
    flex: 1,
    overflow: "scroll",
  },
  full: {
    padding: 16,
    margin: 2,
    flexGrow: 999,
    elevation: 0,
  },
  nested: {
    marginLeft: 16,
  },
  nodePageContainer: {
    padding: 16,
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
});

function renderNodePage(node: Lesson | Example | Definition): ReactNode {
  if (node instanceof Lesson) {
    return <LessonPageRenderer lesson={node} />;
  } else {
    return `Invalid Node ${node}`;
  }
}

export interface IBookRendererProps {
  book: Book;
}

export function BookRenderer(props: IBookRendererProps) {
  const [currentNode, setCurrentNode] = useState<
    Option<Lesson | Example | Definition>
  >(none());

  return (
    <View style={styles.container}>
      <View style={styles.sidebar}>
        <Surface style={styles.full}>
          <List.Section>
            {props.book.getChapters().map((chapter) => (
              <List.Accordion
                key={chapter.getId()}
                title={
                  <>
                    {chapter.getId()} -{" "}
                    <RenderableRenderer node={chapter.getTitle()} />
                  </>
                }
              >
                <List.Section style={styles.nested}>
                  {chapter.getUnits().map((unit) => (
                    <List.Accordion
                      key={unit.getId()}
                      title={
                        <>
                          {unit.getId()} -{" "}
                          <RenderableRenderer node={unit.getTitle()} />
                        </>
                      }
                    >
                      <List.Section style={styles.nested}>
                        {unit.getLessons().map((lesson) => (
                          <List.Accordion
                            key={lesson.getId()}
                            title={
                              <>
                                {lesson.getId()} -{" "}
                                <RenderableRenderer node={lesson.getTitle()} />
                              </>
                            }
                          >
                            <List.Section style={styles.nested}>
                              <List.Accordion title="Definitions">
                                <List.Section style={styles.nested}>
                                  {[...lesson.getDefinitions()].map(
                                    (definition, index) => (
                                      <List.Item
                                        key={index}
                                        title={
                                          <RenderableRenderer
                                            node={definition.getTitle()}
                                          />
                                        }
                                        onPress={() =>
                                          setCurrentNode(some(lesson))
                                        }
                                      />
                                    )
                                  )}
                                </List.Section>
                              </List.Accordion>
                              <List.Accordion title="Examples">
                                <List.Section style={{ marginLeft: 16 }}>
                                  {[...lesson.getExamples()].map(
                                    (example, index) => (
                                      <List.Item
                                        key={index}
                                        title={
                                          <RenderableRenderer
                                            node={example.getTitle()}
                                          />
                                        }
                                        onPress={() =>
                                          setCurrentNode(some(example))
                                        }
                                      />
                                    )
                                  )}
                                </List.Section>
                              </List.Accordion>
                              <List.Item
                                title={
                                  <RenderableRenderer
                                    node={lesson.getTitle()}
                                  />
                                }
                                onPress={() => setCurrentNode(some(lesson))}
                              />
                            </List.Section>
                          </List.Accordion>
                        ))}
                      </List.Section>
                    </List.Accordion>
                  ))}
                </List.Section>
              </List.Accordion>
            ))}
          </List.Section>
        </Surface>
      </View>
      <View style={styles.main}>
        <Surface style={styles.full}>
          {currentNode
            .mapOrElse(
              (node) => (
                <View style={styles.nodePageContainer}>
                  {renderNodePage(node)}
                </View>
              ),
              () => <BookPageRenderer book={props.book} />
            )
            .unwrap()}
        </Surface>
      </View>
    </View>
  );
}
