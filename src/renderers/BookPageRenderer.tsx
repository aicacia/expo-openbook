import { default as React, ReactNode } from "react";
import { View, StyleSheet, Linking } from "react-native";
import { IconButton, Title, Divider, List } from "react-native-paper";
import { Book } from "@aicacia/openbook";
import { RenderableRenderer } from "./RenderableRenderer";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  email: {
    marginLeft: 4,
    marginRight: 2,
  },
});

export interface IBookPageRendererProps {
  book: Book;
}

export function BookPageRenderer(props: IBookPageRendererProps) {
  return (
    <View style={styles.container}>
      <Title>
        <RenderableRenderer node={props.book.getTitle()} />
      </Title>
      <Divider />
      <List.Section>
        {props.book.getAuthors().map((author, index) => (
          <List.Item
            key={index}
            title={<RenderableRenderer node={author.getName()} />}
            right={() => (
              <>
                {author
                  .getEmail()
                  .map(
                    (email) =>
                      (
                        <IconButton
                          style={styles.email}
                          icon="email"
                          onPress={() => Linking.openURL(`mailto:${email}`)}
                        />
                      ) as ReactNode
                  )
                  .unwrapOr(null)}
                {author
                  .getPhone()
                  .map(
                    (phone) =>
                      (
                        <IconButton
                          icon="phone"
                          onPress={() => Linking.openURL(`tel:${phone}`)}
                        />
                      ) as ReactNode
                  )
                  .unwrapOr(null)}
              </>
            )}
          />
        ))}
      </List.Section>
    </View>
  );
}
