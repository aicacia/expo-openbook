import { default as React } from "react";
import { Reference } from "@aicacia/openbook";
import { RenderableRenderer } from "./RenderableRenderer";

export interface IReferenceRendererProps {
  reference: Reference;
}

export function ReferenceRenderer(props: IReferenceRendererProps) {
  return <RenderableRenderer node={props.reference.getNode()} />;
}
