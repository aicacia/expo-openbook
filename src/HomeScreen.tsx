import { default as React } from "react";
import { BookRenderer } from "./renderers/BookRenderer";
import theOpenbookBook from "../the_openbook_book";

export function HomeScreen() {
  return <BookRenderer book={theOpenbookBook} />;
}
