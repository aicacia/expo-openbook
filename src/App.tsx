import { default as React } from "react";
import { StatusBar } from "expo-status-bar";
import { Provider } from "react-native-paper";
import { theme } from "./theme";
import { Navigation } from "./Navigation";

export function App() {
  return (
    <Provider theme={theme}>
      <Navigation />
      <StatusBar style="auto" />
    </Provider>
  );
}
