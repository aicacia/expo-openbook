import { Chapter } from "@aicacia/openbook";
import unit01 from "./01_unit";

export default new Chapter("Intro to OpenBooks").addUnits(unit01);
