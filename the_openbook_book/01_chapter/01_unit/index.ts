import { Unit } from "@aicacia/openbook";
import lesson01 from "./01_lesson";

export default new Unit("Books").addLessons(lesson01);
