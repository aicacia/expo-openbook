import { Lesson } from "@aicacia/openbook";
import { author, book } from "./definitions";
import { usingAnAuthorInABook } from "./examples";

export default new Lesson("Books").addChildren(
  book.getReference(),
  "s are the entry points. A ",
  book.getReference(),
  " Can have one of more ",
  author.getReference(),
  "(s). ",
  usingAnAuthorInABook.getReference()
);
