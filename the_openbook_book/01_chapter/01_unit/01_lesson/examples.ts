import { Text, Code, Block, Example } from "@aicacia/openbook";
import { author, book } from "./definitions";

export const usingAnAuthorInABook = new Example(
  "Using an Author in a Book"
).addChildren(
  new Block().addChildren(
    "Creating an ",
    author.getReference(),
    " in a ",
    book.getReference()
  ),
  new Code(`
  import { Author, Book, Text } from "@aicacia/openbook";

  export default new Book(new Text("The OpenBook Book"))
    .addAuthor(
      new Author(new Text("Nathan Faucett"))
        .setEmail("nathanfaucett@gmail.com")
        .setPhone("1-800-555-1337")
    );
  `).setLanguage("typescript")
);
