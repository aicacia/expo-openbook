import { Definition } from "@aicacia/openbook";

export const author = new Definition("Author");
export const book = new Definition("Book");
