import { Author, Book } from "@aicacia/openbook";
import chapter01 from "./01_chapter";

export default new Book("The OpenBook Book")
  .addAuthors(
    new Author("Nathan Faucett")
      .setEmail("nathanfaucett@gmail.com")
      .setPhone("1-800-555-1337")
  )
  .addChapters(chapter01);
